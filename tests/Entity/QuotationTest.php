<?php

class QuotationTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var \App\Entity\Quotation
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new \App\Entity\Quotation();
    }

    public function testGetterAndSetter() {

        $this->assertNull($this->object->getId());

        $this->object->setCompanySymbol("XMPHP");
        $this->assertEquals("XMPHP", $this->object->getCompanySymbol());

        $date = new \DateTime();

        $this->object->setFromDate($date);
        $this->assertEquals($date, $this->object->getFromDate());

        $this->object->setToDate($date);
        $this->assertEquals($date, $this->object->getToDate());

        $this->object->setEmail("name@domain.com");
        $this->assertEquals("name@domain.com", $this->object->getEmail());
    }
}