<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {
    public function testShowForm() {
        $client = static::createClient();

        $client->request('GET', '/quotation');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}