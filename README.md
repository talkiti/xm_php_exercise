## Solution to XM PHP Exercise - v20.0.0 ##

After downloading the project please use `composer install` to download all packages and modules.

Webserver:
- Docker services exist and may be used with `docker-compose up`
* It is also optional to use any Webserver, just mount the project and start the server up.
** Then navigate in browser to "project folder"/web/index.php/quotation

Docker Services:
- php - php-fpm process (as well as basic image for test execution)
- nginx - www server

Application has been developed with Symfony 4 framework - modern, well designed and modular framework.
Additionally I used a common module (Bundle) to speed up development and avoid or code duplication (DRY principle):
- SwiftmailerBundle for easy mailing 

For testing I selected PHPUnit

Running tests:
`docker-compose run php bin/phpunit`
