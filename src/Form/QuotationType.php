<?php

namespace App\Form;

use App\Entity\Quotation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class QuotationType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('companySymbol', ChoiceType::class, [
                'attr' => ['placeholder' => 'Choose company symbol',],
                'choices' => $options['company_symbols'],
            ])
            ->add('fromDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'jqueryui-datepicker',
                    'placeholder' => 'Enter start date',
                ],
                'html5' => false,
            ])
            ->add('toDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'jqueryui-datepicker',
                    'placeholder' => 'Enter end date',
                ],
                'html5' => false,
            ])
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'Enter your email address to receive an email',],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Quotation::class,
            'company_symbols' => null,
            'company_symbol' => 'AAPL',
            'from_date' => '2016-01-01',
            'to_date' => '2016-02-01',
        ]);
    }
}
