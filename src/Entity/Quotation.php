<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuotationRepository")
 */
class Quotation {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $companySymbol;

    /**
     * @ORM\Column(type="date")
     */
    private $fromDate;

    /**
     * @ORM\Column(type="date")
     */
    private $toDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    public function getId() {
        return $this->id;
    }

    public function getCompanySymbol(): ?string {
        return $this->companySymbol;
    }

    public function setCompanySymbol(string $companySymbol): self {
        $this->companySymbol = $companySymbol;

        return $this;
    }

    public function getFromDate(): ?\DateTimeInterface {
        return $this->fromDate;
    }

    public function setFromDate(\DateTimeInterface $fromDate): self {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getToDate(): ?\DateTimeInterface {
        return $this->toDate;
    }

    public function setToDate(\DateTimeInterface $toDate): self {
        $this->toDate = $toDate;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(?string $email): self {
        $this->email = $email;

        return $this;
    }
}
