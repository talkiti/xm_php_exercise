<?php

namespace App\Controller;

use App\Entity\Quotation;
use App\Form\QuotationType;
//use App\Repository\QuotationRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class QuotationController extends Controller {

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * QuotationController constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory) {
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/quotation", name="quotation")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, \Swift_Mailer $mailer) {
        $companySymbols = $this->getCompanies();
        $form = $this->formFactory->create(QuotationType::class, new Quotation(), ['company_symbols' => $companySymbols]); //symfony 4?

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /* @var $quotation Quotation */
            $quotation = $form->getData();
            if ($this->form_validation($quotation)) {
                $data = $this->getHistoricalQuotes($quotation);
                $tableHtml = $this->drawTable($data, ['Date', 'Open', 'High', 'Low', 'Close']);
                $chartData = $this->prepareChartData($data, 'Date', ['Open', 'Close']);
                $emailStatus = $this->sendEmail($quotation, $mailer);

                return $this->render('quotation/index.html.twig', [
                    'quotationForm' => $form->createView(),
                    'quotationTable' => $tableHtml,
                    'quotationChart' => $chartData,
                    'quotationEmail' => $emailStatus,
                ]);
            } else {
                // show message to user
                echo 'fdsfdsfdsfds';
            }
        }

        return $this->render('quotation/index.html.twig', [
            'quotationForm' => $form->createView(),
        ]);
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     */
    private function makeRequest(string $url, array $params = [], string $method = 'GET') {
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request($method, $url, ['verify' => false]);
            return $res->getBody()->getContents();
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            //notify user
        }
    }

    /**
     * @return array
     */
    private function getCompanies($companySymbol = null) {
        $data = [];
        $url = "https://www.nasdaq.com/screening/companies-by-name.aspx?render=download";
        $response = $this->makeRequest($url);
        $lines = explode(PHP_EOL, $response);
        array_shift($lines); //remove headers
        foreach ($lines as $k => $line) {
            $csvRow = array_filter(str_getcsv($line));
            if (!empty($csvRow)) {
                if(isset($companySymbol) && $companySymbol === $csvRow[0]){
                    return $csvRow[1];
                }
                $data[$csvRow[1]] = $csvRow[0];
            }
        }

        if(isset($companySymbol)){
            return null;
        }
        return $data;
    }

    /**
     * @param Quotation $quotation
     * @return bool
     */
    private function form_validation(Quotation $quotation) {
        $email = filter_var($quotation->getEmail(), FILTER_VALIDATE_EMAIL);

        $company_symbol = strip_tags($quotation->getCompanySymbol());
        $company = $this->getCompanies($company_symbol);
//        $company = filter_var(array_search($company_symbol, $companies), FILTER_VALIDATE_INT);

        $from_date = $quotation->getFromDate();
        $to_date = $quotation->getToDate();

        return $email && $company && $from_date <= $to_date;
    }

    /**
     * @param Quotation $quotation
     * @return array
     */
    private function getHistoricalQuotes(Quotation $quotation) {
        $url = "https://www.quandl.com/api/v3/datasets/WIKI/{$quotation->getCompanySymbol()}.csv?order=asc&start_date={$quotation->getFromDate()->format('Y-m-d')}&end_date={$quotation->getToDate()->format('Y-m-d')}";
        $response = $this->makeRequest($url);

        $lines = explode("\n", $response);
        $headers = str_getcsv(array_shift($lines));
        $quotes = [];
        foreach ($lines as $line) {
            $row = [];
            foreach (str_getcsv($line) as $key => $field) {
                $row[$headers[$key]] = $field;
            }
            $row = array_filter($row);
            $quotes[] = $row;
        }
        return ['headers' => array_filter($headers), 'quotes' => array_filter($quotes)];
    }

    /**
     * @param array $data
     * @param array $columns
     * @return string
     */
    private function drawTable(array $data, array $columns = []) {
        $html = '<table class="table table-sm table-bordered table-striped table-hover text-center"><thead>';
        foreach ($data['headers'] as $key => $header) {
            if (in_array($header, $columns)) {
                $html .= '<th>' . htmlspecialchars($header) . '</th>';
            }
        }
        $html .= '</thead><tbody>';
        foreach ($data['quotes'] as $line) {
            $html .= '<tr>';
            foreach ($columns as $column) {
                $html .= '<td>' . htmlspecialchars($line[$column]) . '</td>';
            }
            $html .= '</tr>';
        }

        return $html . '</tbody></table>';
    }

    /**
     * @param array $data
     * @param string $xAxis
     * @param array $columns
     * @return array
     */
    private function prepareChartData(array $data, string $xAxis, $columns = []) {
        $colors = ['Blue', 'Red', 'Purple', 'Green', 'Yellow'];
        $chartData = [
            'labels' => [],
            'datasets' => [],
        ];
        $datasets = [];
        foreach ($data['quotes'] as $line) {
            $chartData['labels'][] = $line[$xAxis];
            foreach ($columns as $column) {
                if (!isset($datasets[$column])) {
                    $datasets[$column] = [];
                }
                $datasets[$column][] = $line[$column];
            }
        }
        foreach ($datasets as $key => $data) {
            $chartData['datasets'][] = [
                'label' => $key,
                'data' => $data,
                'backgroundColor' => array_pop($colors),
            ];
        }
        return $chartData;
    }


    private function sendEmail(Quotation $quotation, \Swift_Mailer $mailer) {
        $message = (new \Swift_Message())
            ->setFrom('xmphptask@gmail.com')
            ->setTo($quotation->getEmail())
            ->setSubject($this->getCompanies($quotation->getCompanySymbol()))
            ->setBody("From " . $quotation->getFromDate()->format('Y-m-d') . " to " . $quotation->getToDate()->format('Y-m-d'));
        return $mailer->send($message);
    }
}
